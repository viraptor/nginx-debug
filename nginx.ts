module Nginx {
	"use strict";

	enum State {
		SKIP,
		IN_STRING,
		QUOTED,
		TOKEN,
		COMMENT,
		STATEMENT_END,
		BEGIN_BLOCK,
	}

	enum Token {
		END_OF_STATEMENT,
		BEGIN_BLOCK,
		END_BLOCK,
		END_DOCUMENT,
	}

	class Lexer {
		private position: number;
		private state: State;
		private buffer: string;
		private lineno: number;

		constructor(private config: string) {
			this.position = 0;
			this.state = State.SKIP;
			this.lineno = 1;
		}

		next_char(): string {
			if (this.position >= this.config.length) {
				return null;
			}

			let c = this.config[this.position];
			this.position+=1;
			return c;
		}

		next_token(): [number, string | Token] {
			// leftover tokens
			switch (this.state) {
				case State.STATEMENT_END:
					this.state = State.SKIP;
					return [this.lineno, Token.END_OF_STATEMENT];
				case State.BEGIN_BLOCK:
					this.state = State.SKIP;
					return [this.lineno, Token.BEGIN_BLOCK];
			}

			while (true) {
				let c = this.next_char();
				if (c == "\n") this.lineno += 1;
				if (c == null) {
					return [this.lineno, Token.END_DOCUMENT];
				}

				switch(this.state)  {
					case State.SKIP:
						switch(c) {
							// whitespace
							case ' ':
							case "\t":
							case "\n":
								break;

							case ';':
								return [this.lineno, Token.END_OF_STATEMENT];
							case '{':
								return [this.lineno, Token.BEGIN_BLOCK];
							case '}':
								return [this.lineno, Token.END_BLOCK];

							case '#':
								this.state = State.COMMENT;
								break;

							case '"':
								this.state = State.IN_STRING;
								this.buffer = "";
								break;

							default:
								this.buffer = c;
								this.state = State.TOKEN;
								break;
						}
						break;

					case State.COMMENT:
						// eat it until the end of the line
						switch(c) {
							case "\n":
								this.state = State.SKIP;
								break;
							default:
								break;
						}
						break;

					case State.TOKEN:
						switch(c) {
							case ' ':
							case "\t":
							case "\n":
								this.state = State.SKIP;
								return [this.lineno, this.buffer];

							case ';':
								this.state = State.STATEMENT_END;
								return [this.lineno, this.buffer];

							case '{':
								this.state = State.BEGIN_BLOCK;
								return [this.lineno, this.buffer];

							case '#':
								this.state = State.COMMENT;
								return [this.lineno, this.buffer];

							default:
								this.buffer += c;
								break;
						}
						break;

					case State.IN_STRING:
						switch(c) {
							case "\\":
								this.state = State.QUOTED;
								break;

							case '"':
								this.state = State.SKIP;
								return [this.lineno, this.buffer];

							default:
								this.buffer += c;
								break;
						}
						break;

					case State.QUOTED:
						switch(c) {
							case 'n':
								this.buffer += "\n";
								break;
							default:
								this.buffer += c;
								break;
						}
						this.state = State.QUOTED;
						break;
				}
			}
		}
	}

	class Parser {
		private config: any[];
		private statement: any[];
		private statement_lineno: number;

		constructor(private lexer: Lexer, public warnings: string[]) {
			this.config = [];
			this.statement = [];
			this.statement_lineno = null;
		}

		parse(): any[] {
			let st_line = null;
			while (true) {
				let lineno_token = this.lexer.next_token();
				let tok = lineno_token[1];
				switch(tok) {
					case Token.END_DOCUMENT:
						return this.config;

					case Token.BEGIN_BLOCK:
						st_line = this.statement_lineno;
						this.statement_lineno = null;

						let block_parser = new Parser(this.lexer, this.warnings);
						this.statement.push(block_parser.parse());
						this.config.push(this.statement);
						this.statement = [];
						break;

					case Token.END_BLOCK:
						if (this.statement.length > 0) {
							this.config.push(this.statement);
							this.statement = [];
						}
						return this.config;

					case Token.END_OF_STATEMENT:
						st_line = this.statement_lineno;
						this.statement_lineno = null;

						if (this.statement.length == 0)
							break;
						if (this.statement[0] == 'include') {
						this.warnings.push(`line ${st_line}: no files available, didn't include ${this.statement[1]}`);
							this.statement = [];
							break;
						}
						this.config.push(this.statement);
						this.statement = [];
						break;

					default:
						if (!this.statement_lineno) {
							this.statement_lineno = lineno_token[0];
						}
						this.statement.push(tok);
						break;
				}
			}
		}
	}

	enum Match {
		Regex,
		RegexNoCase,
		Prefix,
		PrefixPriority,
		Exact,
	}

	class Server {
		private locations: [string, Match, any[]][];
		private server_name: string[];
		private root: string;
		private index: string[];

		constructor(config: any[]) {
			this.index = [];
			this.locations = [];
			this.server_name = [];

			for (let s of config) {
				switch(s[0]) {
					case 'root':
						this.root = s[1];
						break;

					case 'server_name':
						for (let i in s) {
							if (i==0) continue;
							this.server_name.push(s[i]);
						}
						break;

					case 'index':
						for (let i in s) {
							if (i==0) continue;
							this.index.push(s[i]);
						}
						break;

					case 'location':
						switch (s[1]) {
							case '=':
								this.locations.push([s[2], Match.Exact, s[3]]);
								break;
							case '~':
								this.locations.push([s[2], Match.Regex, s[3]]);
								break;
							case '~*':
								this.locations.push([s[2], Match.RegexNoCase, s[3]]);
								break;
							case '^~':
								this.locations.push([s[2], Match.PrefixPriority, s[3]]);
								break;
							default:
								this.locations.push([s[1], Match.Prefix, s[2]]);
								break;
						}
						break;

					default:
						// ignore unknown lines
						break;
				}
			}
		}

		toString(): string {
			return `[Server name: ${this.server_name} root: ${this.root} locations: ${this.locations.length}]`;
		}

		serverMatch(url: URL): number[][] {
			function suffix_match(hostname: string, given: string): boolean {
				if (hostname[0] != '*') return false;
				return given.endsWith(hostname.substr(1));
			}
			function prefix_match(hostname: string, given: string): boolean {
				if (hostname[hostname.length-1] != '*') return false;
				return given.startsWith(hostname.substr(0,hostname.length-2));
			}

			let matches: number[][] = [];
			for (let name of this.server_name) {
				if (url.hostname == name) {
					matches.push([4]);
				} else if(suffix_match(name, url.hostname)) {
					matches.push([3, name.length]);
				} else if(prefix_match(name, url.hostname)) {
					matches.push([2, name.length]);
				} // TODO regex match
			}
			return matches;
		}

		locationMatch(url: URL): [[string, Match],[string, Match][]] {
			// collect valid matches in order of checking, chosen or not
			let results: [string, Match][] = [];

			// check for exact matches
			for (let loc of this.locations) {
				if (loc[1] != Match.Exact) continue;
				if (loc[0] == url.pathname) {
					results.push([loc[0], loc[1]]);
					return [[loc[0], loc[1]], results];
				}
			}

			// check prefixes
			let best_match = null;
			let best_length = 0;
			for (let loc of this.locations) {
				if (loc[1] != Match.Prefix && loc[1] != Match.PrefixPriority) continue;
				if (url.pathname.startsWith(loc[0])) {
					results.push([loc[0], loc[1]]);
					if (loc[0].length > best_length) {
						best_match = loc;
						best_length = loc[0].length;
					}
				}
			}

			// for priority prefix match, don't go on to regex matching
			if (best_match && best_match[1] == Match.PrefixPriority) {
				return [[best_match[0], best_match[1]], results];
			}

			// regex match
			for (let loc of this.locations) {
				if (loc[1] == Match.Regex || loc[1] == Match.RegexNoCase) {
					let options = "";
					if (loc[1] == Match.RegexNoCase)
						options = "i";
					let re = new RegExp(loc[0], options);
					if (url.pathname.search(re) > -1) {
						results.push([loc[0], loc[1]]);
						return [[loc[0], loc[1]], results];
					}
				}
			}

			// regex failed, use the stored longest match
			if (best_match) {
				return [[best_match[0], best_match[1]], results];
			}

			// no previous match
			return [null, results];
		}

		get serverNames(): string[] {
			return this.server_name;
		}
	}

	class CheckResult {
		constructor(public server: Server, public server_score: number[],
			public location: string, public location_type: Match,
			public checked_matching: [string, Match][]) {
		}
	}

	class Config {
		public got_servers: boolean;
		public servers: Server[];

		constructor(private config: any[]) {
			this.got_servers = false;
			this.servers = [];

			// find http block if available
			for (let b of config) {
				if (b[0] == 'http') {
					config = b[1];
					break;
				}
			}

			// are servers there?
			for (let b of config) {
				if (b[0] == 'server') {
					this.servers.push(new Server(b[1]));
					this.got_servers = true;
				}
			}

			if (!this.got_servers) {
				// whole block is a server, just include it that way
				this.servers.push(new Server(this.config));
			}
		}

		toString(): string {
			let res = "[";
			for (let s of this.servers) {
				res += s.toString();
				res += ', ';
			}
			res += "]";
			return res;
		}

		checkUrl(url: string): CheckResult {
			let target = new URL(url);
			let best_server: Server = null;
			let best_score: number[] = [0];

			if (this.got_servers) {
				for (let server of this.servers) {
					let scores = server.serverMatch(target);
					for (let score of scores) {
						if (best_score[0] < score[0]) {
							best_score = score;
							best_server = server;
							continue;
						}
						if (best_score[0] == score[0] && score.length>1 && best_score[1] < score[1]) {
							best_score = score;
							best_server = server;
							continue;
						}
					}
				}
				console.log(`Selected server ${best_server.toString()} with score ${best_score}`);
			} else {
				best_server = this.servers[0];
				console.log("Only one server defined");
			}

			let res = best_server.locationMatch(target);
			let loc_string = res[0][0];
			let loc_type = res[0][1];
			let all_matches = res[1];
			console.log(`Selected location ${loc_string}`);

			return new CheckResult(best_server, best_score, loc_string, loc_type, all_matches);
		}
	}

	function matchTypeToString(match: Match): string {
		switch(match) {
		case Match.Exact: return "exact";
		case Match.Regex: return "case sensitive regex";
		case Match.RegexNoCase: return "case insensitive regex";
		case Match.Prefix: return "prefix";
		case Match.PrefixPriority: return "priority prefix";
		}
	}

	function resultsAsList(result: CheckResult): Node {
		let container = document.createElement('div');
		let header = document.createElement('h2');
		header.appendChild(document.createTextNode("Locations which were matched while searching"));
		container.appendChild(header);

		let list = document.createElement('ol');
		for (let m of result.checked_matching) {
			let entry = document.createElement('li');
			let description = document.createTextNode(`${matchTypeToString(m[1])} match for location ${m[0]}`);
			entry.appendChild(description);
			list.appendChild(entry);
		}
		container.appendChild(list);
		return container;
	}

	function removeChildren(node: Node) {
		while(node.lastChild) {
			node.removeChild(node.lastChild);
		}
	}

	function configDescription(config: Config): Node {
		let container = document.createElement('div');
		let header = document.createElement('h2');
		header.appendChild(document.createTextNode("Config"));
		container.appendChild(header);

		let servers = document.createElement('p');
		if (config.got_servers) {
			servers.appendChild(document.createTextNode(`Found ${config.servers.length} server(s) defined. Attempted server name matching.`));
		} else {
			servers.appendChild(document.createTextNode("No server blocks found. Will match only on path component."));
		}
		container.appendChild(servers);
		return container;
	}
	
	function serverMatch(result: CheckResult): Node {
		let container = document.createElement('div');
		let header = document.createElement('h2');
		header.appendChild(document.createTextNode("Server match"));
		container.appendChild(header);

		let server = document.createElement('p');
		if (result.server) {
			server.appendChild(document.createTextNode(`Matched server with names: ${result.server.serverNames}.`));
		} else {
			server.appendChild(document.createTextNode(`Did not match any server.`));
		}
		container.appendChild(server);

		return container;
	}

	function parseWarnings(parser: Parser): Node {
		let container = document.createElement('div');
		let header = document.createElement('h2');
		header.appendChild(document.createTextNode("Config processing warnings"));
		container.appendChild(header);

		let list = document.createElement('ul');
		for (let warning of parser.warnings) {
			let entry = document.createElement('li');
			let description = document.createTextNode(warning);
			entry.appendChild(description);
			list.appendChild(entry);
		}
		container.appendChild(list);

		return container;
	}

	function matchDescription(result: CheckResult): Node {
		let container = document.createElement('div');
		let header = document.createElement('h2');
		header.appendChild(document.createTextNode("Final/chosen match"));
		container.appendChild(header);

		let location = document.createElement('p');
		location.appendChild(document.createTextNode(`Location: ${result.location}`));
		container.appendChild(location);

		let match = document.createElement('p');
		match.appendChild(document.createTextNode(`Match type: ${matchTypeToString(result.location_type)}`));
		container.appendChild(match);

		return container;
	}

	export function runCheck(src: any, dest: any, url: string) {
		let p = new Parser(new Lexer(src.value), []);
		let config = new Config(p.parse());

		let res = config.checkUrl(url);
		removeChildren(dest);
		dest.appendChild(configDescription(config));
		if (p.warnings.length > 0) {
			dest.appendChild(parseWarnings(p));
		}
		if (config.got_servers) {
			dest.appendChild(serverMatch(res));
		}
		dest.appendChild(resultsAsList(res));
		dest.appendChild(matchDescription(res));
	}
}
